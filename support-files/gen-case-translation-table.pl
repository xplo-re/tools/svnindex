#!/usr/bin/perl
# -*- Mode: Perl; tab-width: 4; indent-tabs-mode: nil; coding: utf-8 -*-
#
# xplo.re svnindex
#
# Case translation table support script for XSLT 1.0.
#
# Copyright (c) 2011, xplo.re IT Services, Michael Maier.
# All rights reserved.
#

use strict;
use utf8;
use warnings;

# Configuration.
my $UNIVER = "6.0";                     # Unicode version to use.

#-----------------------------------------------------------------------------
sub trim($);
sub unihex2utf8($);

#-----------------------------------------------------------------------------
binmode (STDERR, ":utf8");
binmode (STDOUT, ":utf8");

print STDERR <<"EOS";
xplo.re Case Translation Table Generator from Unicode Data
Copyright (c) 2011, xplo.re IT Services, Michael Maier"
All rights reserved.

EOS

sub usage
{
    print STDERR <<"EOS";
usage: `basename "$0"` >output-data.txt

  The generated output uses UTF-8 encoding and contains two lines of letters
  designated for use with the XSLT translate() function.
  Line 1 contains all lowercase letters; line 2 all uppercase variants. Only
  simple case folding is supported, hence the C and S types from the Unicode
  case folding summary are used; full case folding is not compatible with the
  XSLT 1.0 translate() function and would be insufficient if implemented via
  standard string replacement methods.
EOS
}

if (shift)
{
    usage;
    exit;
}

#-----------------------------------------------------------------------------
my $ROOT = trim `dirname "$0"`;
my $UCCF = "$ROOT/Unicode/$UNIVER/CaseFolding.txt";

if (! -r "$UCCF")
{
    print STDERR "error: cannot read Unicode case folding document";
    exit (-2);
}

open (UCCF, "<$UCCF")
    or die ("error: failed to open Unicode case folding document for reading");

my $lowerstr;
my $upperstr;

while (<UCCF>)
{
    if (/^([0-9a-fA-F]+);\s*[SC];\s*([0-9a-fA-F]+);\s*#\s*([A-Z ]*)/)
    {
        my $lower = unihex2utf8 ($2);   # pack ('U', hex ($1));
        my $upper = unihex2utf8 ($1);   # pack ('U', hex ($2));

        print STDERR "info: mapping »$lower« => »$upper«  ($3)\n";

        $lowerstr .= $lower;
        $upperstr .= $upper;
    }
}

print "\t<xsl:variable name=\"gen-letter-lowercase\">$lowerstr</xsl:variable>\n" .
      "\t<xsl:variable name=\"gen-letter-uppercase\">$upperstr</xsl:variable>\n";

#-----------------------------------------------------------------------------
# Support function.
sub trim($)
{
    my $str = shift;

    $str =~ s/^\s+//;
    $str =~ s/\s+$//;

    return ($str);
}

sub unihex2utf8($)
{
    my $unicode =
        hex (shift);

    return
            ((($unicode >= 32) && ($unicode < 128)) || ($unicode > 160))
        ?   chr ($unicode)
        :   sprintf ("\\x{%X}", $unicode);
}
