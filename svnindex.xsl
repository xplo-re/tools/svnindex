<?xml version="1.0" encoding="utf-8"?>
<!-- -*- Mode: XSL; tab-width: 4; indent-tabs-mode: on; coding: utf-8 -*- -->
<!--
<! xplo.re svnindex
<!
<! Copyright (c) 2006, xplo.re IT Services, Michael Maier.
<! All rights reserved.
<!-->

<xsl:stylesheet version="1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="html"
	>

	<!-- XHTML output in standard UTF-8 encoding. -->
	<xsl:output
		method="xml" version="1.0" encoding="utf-8" indent="yes"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
		doctype-public="-//W3C//DTD XHTML 1.0 STRICT//EN"
		/>

	<!-- There are no special aligned whitespace-tags, thus strip whitespaces. -->
	<xsl:strip-space elements="*" />

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- Globals. -->
	<xsl:variable name="path-prefix">/xsl</xsl:variable>
	<xsl:variable name="support-contact">mailto:support@xplo-re.org</xsl:variable>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- SVN (entry). -->
	<xsl:template match="svn">
		<html>
			<head>
				<title>
					<xsl:value-of select="index/@base" />
					<xsl:text>:</xsl:text>
					<xsl:value-of select="index/@path" />
					<xsl:if test="string-length(index/@name) != 0">
						<xsl:text> &#8212; </xsl:text>
						<xsl:value-of select="index/@name" />
					</xsl:if>
				</title>
				<link rel="stylesheet" type="text/css" href="{$path-prefix}/rsrc/svnindex.css" />
			</head>
			<body>
				<div id="header">
					<xsl:value-of select="index/@name" />
					<span class="right">
						<xsl:text> Repository Browser</xsl:text>
					</span>
				</div>
				<xsl:if test="index/@rev &gt; 0">
					<div id="revision">
						<span>Head Revision</span>
						<xsl:value-of select="index/@rev" />
					</div>
				</xsl:if>
				<xsl:apply-templates select="index" />
				<div id="footer">
					<div class="left">
						<xsl:text>Powered by </xsl:text>
						<xsl:element name="a">
							<xsl:attribute name="href">
								<xsl:text>http://xplo.re/</xsl:text>
							</xsl:attribute>
							<xsl:text>xplo.re IT Services svnindex</xsl:text>
						</xsl:element>
						<xsl:text>, </xsl:text>
						<xsl:element name="a">
							<xsl:attribute name="href">
								<xsl:value-of select="@href" />
							</xsl:attribute>
							<xsl:text>Subversion</xsl:text>
						</xsl:element>
						<xsl:text> v</xsl:text>
						<xsl:value-of select="@version" />
					</div>
					<div class="right">
						<xsl:element name="a">
							<xsl:attribute name="href">
								<xsl:value-of select="$support-contact" />
							</xsl:attribute>
							<xsl:text>Technical Assistance and Support</xsl:text>
						</xsl:element>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- INDEX. -->
	<xsl:template match="index">
		<div id="location">
			<xsl:value-of select="@base" />
			<xsl:text>: </xsl:text>
			<b><xsl:value-of select="@path" /></b>
		</div>
		<div id="listing">
			<table>
				<tbody>
					<xsl:apply-templates select="updir" />
					<xsl:apply-templates select="dir" />
					<xsl:apply-templates select="file" />
				</tbody>
			</table>
		</div>
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- UPDIR. -->
	<xsl:template match="updir">
		<tr class="updir">
			<td>
				<a href="../">
					<xsl:text>Parent</xsl:text>
				</a>
			</td>
		</tr>
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- DIR. -->
	<xsl:template match="dir">
		<xsl:variable name="lowercase-name">
			<xsl:call-template name="strtolower">
				<xsl:with-param name="string" select="@name" />
			</xsl:call-template>
		</xsl:variable>
		<tr>
			<xsl:attribute name="class">
				<xsl:text>dir </xsl:text>
				<xsl:value-of select="translate($lowercase-name,' &lt;&gt;','___')" />
			</xsl:attribute>
			<td>
				<a href="{@href}">
					<xsl:value-of select="@name" />
					<xsl:text>/</xsl:text>
				</a>
			</td>
		</tr>
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- INDEX. -->
	<xsl:template match="file">
		<xsl:variable name="lowercase-name">
			<xsl:call-template name="strtolower">
				<xsl:with-param name="string" select="@name" />
			</xsl:call-template>
		</xsl:variable>
		<tr>
			<xsl:attribute name="class">
				<xsl:text>file </xsl:text>
				<xsl:value-of select="translate($lowercase-name,' &lt;&gt;','___')" />
			</xsl:attribute>
			<td>
				<xsl:attribute name="style">
					<xsl:choose>
						<xsl:when test="substring($lowercase-name,string-length($lowercase-name)-3,4) = '.ico' or substring($lowercase-name,1,8) = 'favicon.'">
							<xsl:text>background-image: url(</xsl:text><xsl:value-of select="@href" /><xsl:text>);</xsl:text>
						</xsl:when>
					</xsl:choose>
				</xsl:attribute>
				<a href="{@href}">
					<xsl:value-of select="@name" />
				</a>
			</td>
		</tr>
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- Library: String to lowercase. -->
	<xsl:template name="strtolower">
		<xsl:param name="string" />

		<xsl:value-of select="translate($string, $gen-letter-uppercase, $gen-letter-lowercase)" />
	</xsl:template>

	<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
	<!-- Library: String to uppercase. -->
	<xsl:template name="strtoupper">
		<xsl:param name="string" />

		<xsl:value-of select="translate($string, $gen-letter-lowercase, $gen-letter-uppercase)" />
	</xsl:template>

	<xsl:variable name="gen-letter-lowercase">abcdefghijklmnopqrstuvwxyzμàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþāăąćĉċčďđēĕėęěĝğġģĥħĩīĭįĳĵķĺļľŀłńņňŋōŏőœŕŗřśŝşšţťŧũūŭůűųŵŷÿźżžsɓƃƅɔƈɖɗƌǝəɛƒɠɣɩɨƙɯɲɵơƣƥʀƨʃƭʈưʊʋƴƶʒƹƽǆǆǉǉǌǌǎǐǒǔǖǘǚǜǟǡǣǥǧǩǫǭǯǳǳǵƕƿǹǻǽǿȁȃȅȇȉȋȍȏȑȓȕȗșțȝȟƞȣȥȧȩȫȭȯȱȳⱥȼƚⱦɂƀʉʌɇɉɋɍɏιͱͳͷάέήίόύώαβγδεζηθικλμνξοπρστυφχψωϊϋσϗβθφπϙϛϝϟϡϣϥϧϩϫϭϯκρθεϸϲϻͻͼͽѐёђѓєѕіїјљњћќѝўџабвгдежзийклмнопрстуфхцчшщъыьэюяѡѣѥѧѩѫѭѯѱѳѵѷѹѻѽѿҁҋҍҏґғҕҗҙқҝҟҡңҥҧҩҫҭүұҳҵҷҹһҽҿӏӂӄӆӈӊӌӎӑӓӕӗәӛӝӟӡӣӥӧөӫӭӯӱӳӵӷӹӻӽӿԁԃԅԇԉԋԍԏԑԓԕԗԙԛԝԟԡԣԥԧաբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքօֆⴀⴁⴂⴃⴄⴅⴆⴇⴈⴉⴊⴋⴌⴍⴎⴏⴐⴑⴒⴓⴔⴕⴖⴗⴘⴙⴚⴛⴜⴝⴞⴟⴠⴡⴢⴣⴤⴥḁḃḅḇḉḋḍḏḑḓḕḗḙḛḝḟḡḣḥḧḩḫḭḯḱḳḵḷḹḻḽḿṁṃṅṇṉṋṍṏṑṓṕṗṙṛṝṟṡṣṥṧṩṫṭṯṱṳṵṷṹṻṽṿẁẃẅẇẉẋẍẏẑẓẕṡßạảấầẩẫậắằẳẵặẹẻẽếềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹỻỽỿἀἁἂἃἄἅἆἇἐἑἒἓἔἕἠἡἢἣἤἥἦἧἰἱἲἳἴἵἶἷὀὁὂὃὄὅὑὓὕὗὠὡὢὣὤὥὦὧᾀᾁᾂᾃᾄᾅᾆᾇᾐᾑᾒᾓᾔᾕᾖᾗᾠᾡᾢᾣᾤᾥᾦᾧᾰᾱὰάᾳιὲέὴήῃῐῑὶίῠῡὺύῥὸόὼώῳωkåⅎⅰⅱⅲⅳⅴⅵⅶⅷⅸⅹⅺⅻⅼⅽⅾⅿↄⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩⰰⰱⰲⰳⰴⰵⰶⰷⰸⰹⰺⰻⰼⰽⰾⰿⱀⱁⱂⱃⱄⱅⱆⱇⱈⱉⱊⱋⱌⱍⱎⱏⱐⱑⱒⱓⱔⱕⱖⱗⱘⱙⱚⱛⱜⱝⱞⱡɫᵽɽⱨⱪⱬɑɱɐɒⱳⱶȿɀⲁⲃⲅⲇⲉⲋⲍⲏⲑⲓⲕⲗⲙⲛⲝⲟⲡⲣⲥⲧⲩⲫⲭⲯⲱⲳⲵⲷⲹⲻⲽⲿⳁⳃⳅⳇⳉⳋⳍⳏⳑⳓⳕⳗⳙⳛⳝⳟⳡⳣⳬⳮꙁꙃꙅꙇꙉꙋꙍꙏꙑꙓꙕꙗꙙꙛꙝꙟꙡꙣꙥꙧꙩꙫꙭꚁꚃꚅꚇꚉꚋꚍꚏꚑꚓꚕꚗꜣꜥꜧꜩꜫꜭꜯꜳꜵꜷꜹꜻꜽꜿꝁꝃꝅꝇꝉꝋꝍꝏꝑꝓꝕꝗꝙꝛꝝꝟꝡꝣꝥꝧꝩꝫꝭꝯꝺꝼᵹꝿꞁꞃꞅꞇꞌɥꞑꞡꞣꞥꞧꞩａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя</xsl:variable>
	<xsl:variable name="gen-letter-uppercase">ABCDEFGHIJKLMNOPQRSTUVWXYZµÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĀĂĄĆĈĊČĎĐĒĔĖĘĚĜĞĠĢĤĦĨĪĬĮĲĴĶĹĻĽĿŁŃŅŇŊŌŎŐŒŔŖŘŚŜŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽſƁƂƄƆƇƉƊƋƎƏƐƑƓƔƖƗƘƜƝƟƠƢƤƦƧƩƬƮƯƱƲƳƵƷƸƼǄǅǇǈǊǋǍǏǑǓǕǗǙǛǞǠǢǤǦǨǪǬǮǱǲǴǶǷǸǺǼǾȀȂȄȆȈȊȌȎȐȒȔȖȘȚȜȞȠȢȤȦȨȪȬȮȰȲȺȻȽȾɁɃɄɅɆɈɊɌɎͅͰͲͶΆΈΉΊΌΎΏΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫςϏϐϑϕϖϘϚϜϞϠϢϤϦϨϪϬϮϰϱϴϵϷϹϺϽϾϿЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯѠѢѤѦѨѪѬѮѰѲѴѶѸѺѼѾҀҊҌҎҐҒҔҖҘҚҜҞҠҢҤҦҨҪҬҮҰҲҴҶҸҺҼҾӀӁӃӅӇӉӋӍӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸӺӼӾԀԂԄԆԈԊԌԎԐԒԔԖԘԚԜԞԠԢԤԦԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖႠႡႢႣႤႥႦႧႨႩႪႫႬႭႮႯႰႱႲႳႴႵႶႷႸႹႺႻႼႽႾႿჀჁჂჃჄჅḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠḢḤḦḨḪḬḮḰḲḴḶḸḺḼḾṀṂṄṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦṨṪṬṮṰṲṴṶṸṺṼṾẀẂẄẆẈẊẌẎẐẒẔẛẞẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸỺỼỾἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼιῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼΩKÅℲⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩⅪⅫⅬⅭⅮⅯↃⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏⰀⰁⰂⰃⰄⰅⰆⰇⰈⰉⰊⰋⰌⰍⰎⰏⰐⰑⰒⰓⰔⰕⰖⰗⰘⰙⰚⰛⰜⰝⰞⰟⰠⰡⰢⰣⰤⰥⰦⰧⰨⰩⰪⰫⰬⰭⰮⱠⱢⱣⱤⱧⱩⱫⱭⱮⱯⱰⱲⱵⱾⱿⲀⲂⲄⲆⲈⲊⲌⲎⲐⲒⲔⲖⲘⲚⲜⲞⲠⲢⲤⲦⲨⲪⲬⲮⲰⲲⲴⲶⲸⲺⲼⲾⳀⳂⳄⳆⳈⳊⳌⳎⳐⳒⳔⳖⳘⳚⳜⳞⳠⳢⳫⳭꙀꙂꙄꙆꙈꙊꙌꙎꙐꙒꙔꙖꙘꙚꙜꙞꙠꙢꙤꙦꙨꙪꙬꚀꚂꚄꚆꚈꚊꚌꚎꚐꚒꚔꚖꜢꜤꜦꜨꜪꜬꜮꜲꜴꜶꜸꜺꜼꜾꝀꝂꝄꝆꝈꝊꝌꝎꝐꝒꝔꝖꝘꝚꝜꝞꝠꝢꝤꝦꝨꝪꝬꝮꝹꝻꝽꝾꞀꞂꞄꞆꞋꞍꞐꞠꞢꞤꞦꞨＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧ</xsl:variable>
</xsl:stylesheet>